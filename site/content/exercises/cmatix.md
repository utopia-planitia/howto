+++
title = "cmatrix"
weight = 2
+++

## What the Matrix

![matrix letters](https://raw.githubusercontent.com/abishekvashok/cmatrix/master/data/img/capture_orig.gif)

## Tasks

### 1. containerize

Create a Dockerfile to build and run https://github.com/abishekvashok/cmatrix

### 2. minimize image size

Use [Multi stage Docker builds](https://medium.com/travis-on-docker/triple-stage-docker-builds-with-go-and-angular-1b7d2006cb88) to minimze the size of the final image.

