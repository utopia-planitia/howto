+++
title = "dockercraft"
weight = 3
+++

## Minecraft as a UI for Docker

![dockercraft](https://github.com/docker/dockercraft/raw/master/docs/img/dockercraft.gif?raw=true)

## Tasks

### 1. Play and have fun

Checkout https://github.com/docker/dockercraft and have a look at docker minecraft style
