+++
title = "Explain Shell"
+++

[Explain shell](https://explainshell.com/) is a website to explain a shell commands.

example: https://explainshell.com/explain?cmd=ssh+user%40server.tld+-L+8443%3A192.168.200.1%3A443

![The shell has spoken!](http://s2.quickmeme.com/img/18/18c0aae775daa798d9f685ad3b4cc50b67da0751e585be5428ab92b1368f0e7e.jpg)