+++
title = "Testing Regex"
+++

[Online Regex Tester](https://www.regextester.com/) tests Regex patterns.

example: https://www.regextester.com/22

![xkcd humor](https://imgs.xkcd.com/comics/regular_expressions.png)
