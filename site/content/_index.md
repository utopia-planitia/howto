---
title: "Howto CICD"
---

# Howto use Docker & Kubernetes for CI

This repository targets developers, who want to learn about docker and kubernetes.  
It aims to show the use cases of docker, docker-compose, kubernetes and ansible.

{{< tweet 940286691495030784 >}}
