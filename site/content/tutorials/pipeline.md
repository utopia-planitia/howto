+++
title = "Automated builds with Gitlab Ci"
weight = 9
+++

Gitlab watches repositories for [.gitlab-ci.yml](https://docs.gitlab.com/ce/ci/yaml/README.html) files.  
The tasks defined in .gitlab-ci.yml get executed.  
Gitlab sends the request for execution to the Kubernetes-Gitlab-CI-Worker.  
The CI-Worker transforms the request to a kubernetes deployment and sends it to kubernetes.  
As long as kubernetes has enough resources left the tasks gets started.  
Deployments without or to low resource request will potentially get undeployed until enough resources are free again.  

![can't we automate this?](https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAfNAAAAJDVkZmI0NGM4LTE0OGItNDlhOC1iNmVjLTQ1NGE3NzQ0YmI3Zg.jpg)

## Stages

A Pipeline build is seperated into stages.  
Every stage can have multiple parallel tasks.  
The next stage gets only started once all tasks are finished.  
If a tasks fails the pipeline gets aborted.  
Tasks can be defined to be always execute (for cleanup), allow to fail or be retried up to 2 times.  

## Tasks

Every task defines an container image to use as an environment.  
A tasks enviroment can be joined by services.  
System variables can be set to configured the task and the joined services.  
Every tasks can consist of multiple commands.  
Once a command fails, the task aborts.  
The command in after_script get always executed.

## Artifacts

Artifacts defined by a task get send to the Gitlab server.  
Every following tasks downloads the artifact before executing the commands.  
A task can opt out of the artifacts of specify a subset of artifacts to load.

## Environment variables

Gitlab provides [GitLab CI/CD Variables](https://docs.gitlab.com/ce/ci/variables/README.html#gitlab-ci-cd-variables) with config about the build, project and repository.

The variable DOCKER_REGISTRY_INTERNAL is prepared to point to the internal service endpoint of a docker registry.

In addition the Variables are set to configure the docker client to connect to the persistent docker in docker service.

## Example

```
stages:
  - test

hello-world:
  stage: test
  image: alpine:3.6
  script:
    - date
    - echo Hello, world!
    - date
    - echo Goodbye, world.
    - date

pull-push:
  stage: test
  image: docker:17.11.0-ce
  script:
    - docker pull alpine:3.6
    - docker tag alpine:3.6 ${DOCKER_REGISTRY_INTERNAL}/test-pipeline/alpine
    - docker push ${DOCKER_REGISTRY_INTERNAL}/test-pipeline/alpine

run:
  stage: test
  image: docker:17.11.0-ce
  script:
    - docker run --rm hello-world

build:
  stage: test
  image: docker:17.11.0-ce
  script:
    - docker build .
```