+++
title = "manage Docker compose setups"
weight = 3
+++

## start
to start all containers defined in docker-compose.yaml
```
docker-compose up
```
to start all containers in the background add `-d`

## status
show the running containerss
```
docker-compose ps
```

## restart
restart the running setup
```
docker-compose restart
```

## stop
stop the running setup
```
docker-compose stop
```
to add a 1 secound limit to the shutdown time for the processes add `-t 1`

## remove
to remove a stoped container
```
docker-compose down	--remove-orphans
```

## local build image
local Dockerfiles can be used to build an image instead of using a prepared image
```
build: docker/nginx
```
vs
```
image: nginx
```

to update locally build images extend the `up` with `--build`
```
docker-compose up --build
```

![good news for windows](../good-news.jpeg)