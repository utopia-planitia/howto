+++
title = "Infrastructure as code"
weight = 1
+++

The idea behind infrastucture as code is to document not only the projects source code via git,  
but also to document and version the server environment and the steps to build, test and publish a deployment.

<img src="https://i.ytimg.com/vi/KD1JHpzKW3Q/maxresdefault.jpg" width="700">

## Wordpress setup

### local deployment

The local deployment is used to remove bugs and ~~add bugs~~ develop new features.

#### Makefile
make uses a [Makefile](http://mrbook.org/blog/tutorials/make/) to define its tasks to run
{{< highlight make >}}
.PHONY: start
background: ##@development start local server in background
	docker-compose up --build -d

.PHONY: stop
stop: ##@development stop local server
	docker-compose stop -t 1

.PHONY: clean
clean: stop ##@development stop and remove local server
	docker-compose down	--remove-orphans

.PHONY: cli
cli: ##@development open command line interface
	docker-compose exec wordpress sh

.PHONY: logs
logs: ##@development show logs
	docker-compose logs -f
{{< /highlight >}}

#### Docker-compose.yaml
docker-compose uses [docker-compose.yaml](https://docs.docker.com/compose/compose-file/) to define the desired state of the local deployment 
{{< highlight yml >}}
version: '3.0'

services:
  blog:
    image: wordpress:4.9.1-php7.2-apache
    ports:
      - '8080:80'
    links:
      - database:mysql
  database:
    image: mysql:8.0.3
    environment:
      MYSQL_ROOT_PASSWORD: password123
{{< /highlight >}}

### remote deployment

Per pipeline build two deployments are triggered.  
One deployment is created to be used for automated tests  
and a second deployment is created for humans to review manually.

#### .gitlab-ci.yml
Gitlab uses [.gitlab-ci.yml](https://docs.gitlab.com/ce/ci/yaml/README.html) to execute the pipeline steps.

{{< highlight yml >}}
stages:
  - publish

deploy:
  stage: publish
  image: ubuntu:17.10
  script:
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.9.0/bin/linux/amd64/kubectl
    - chmod +x ./kubectl
    - mv ./kubectl /usr/local/bin/kubectl
    - kubectl apply -f wordpress.yaml
  only:
    - master
{{< /highlight >}}


#### wordpress.yaml
kubectl uses [kubernetes yaml definitions](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) configure Kubernetes desired state.

{{< highlight yml >}}
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: blog
spec:
  replicas: 1
  selector:
    matchLabels:
      app: blog
  template:
    metadata:
      labels:
        app: blog
    spec:
      containers:
      - name: blog
        image: wordpress:4.9.1-php7.2-apache
        env:
          - name: WORDPRESS_DB_HOST
            value: 127.0.0.1 # not localhost because of https://stackoverflow.com/questions/3715925/localhost-vs-127-0-0-1/
          - name: WORDPRESS_DB_PASSWORD
            value: password123
        ports:
        - containerPort: 80
      - name: database
        image: mysql:8.0.3
        env:
          - name: MYSQL_ROOT_PASSWORD
            value: password123
{{< /highlight >}}
