+++
title = "use kubernetes pod as proxy to hidden host"
linkTitle = "use pod as proxy"
weight = 13
+++

Sometimes pods are connected to hosts only accessible from inside the deployment (i.e. DB, queues). 
When one wants to access this "hidden" host from the local machine any pod connected to this host can be used as proxy.

## prepare the pod (to be proxy)
Execute the following on the pod.

* install **socat** on the pod: 
`apk add --no-cache socat`

* expose a connection to the **hidden host** on the pod: `socat tcp-l:<exposed_port>,fork,reuseaddr tcp:<hiddenhost_name>:<hiddenhost_port>`

## connect to the hidden host

Execute the following on your local machine. 

* kubernetes port forward to exposed_port: `kubectl -n <namespace> port-forward <podname> <local_port>:<exposed_port>`

## access the hidden host from local machine
the hidden (not anymore) host is now accessible via **localhost:local_port**
