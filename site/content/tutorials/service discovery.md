+++
title = "Service discovery"
weight = 7
+++

Services connect applications using the network.

## Docker
docker is able to link containers to each other via the --link parameter.
```
docker run --name database_container_name -d -e MYSQL_ROOT_PASSWORD=password123 mysql
docker run --name blog_container_name -d --link database_container_name:mysql wordpress
```

The blog container is setup by docker in a way to find the container database_container_name under the name mysql.

## Docker-compose
docker-compose is able to link containers to each other via the links configuration.
```
version: '3.0'

services:
  blog:
    image: wordpress:4.9.1-php7.2-apache
    ports:
      - '8080:80'
    links:
      - database:mysql
  database:
    image: mysql:8.0.3
    environment:
      MYSQL_ROOT_PASSWORD: password123
```
The blog container is setup by docker in a way to find the container database_container_name under the name mysql.

## Kubernetes


### Pod internal

Within a pod all containers share the network.  
The idea is to have service working close together deployed pysically close to each other  
and not create avoidable network traffic in the datacenter.

Notice: PHP trys to connect via the local (almost never shared between containers) unix socket when configured to connect via the network to localhost.

```
apiVersion: v1
kind: Service
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  ports:
    - port: 80
  selector:
    app: wordpress
    tier: frontend
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: wordpress
        tier: frontend
    spec:
      containers:
      - image: wordpress:4.8.0-apache
        name: wordpress
        env:
        - name: WORDPRESS_DB_HOST
          value: 127.0.0.1
        - name: WORDPRESS_DB_PASSWORD
          value: password123
        ports:
        - containerPort: 80
          name: wordpress
      - image: mysql:8.0.3
        name: mysql
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: password123
``` 

### Network

Kubernetes as the notion of a service.  
A Service has a name and a pod selector defined.  
A random virtual ip gets assinged to the service.
Within the cluster a DNS server is deployed. The internal DNS server resolves the service names to the service ips.
The network traffic send to the virtual ip gets redirected (by kube-proxy) to the ips of the pods, selected by the service.

```
apiVersion: v1
kind: Service
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  ports:
    - port: 80
  selector:
    app: wordpress
    tier: frontend
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: wordpress
        tier: frontend
    spec:
      containers:
      - image: wordpress:4.8.0-apache
        name: wordpress
        env:
        - name: WORDPRESS_DB_HOST
          value: wordpress-mysql
        - name: WORDPRESS_DB_PASSWORD
          value: password123
        ports:
        - containerPort: 80
          name: wordpress
---
apiVersion: v1
kind: Service
metadata:
  name: wordpress-mysql
  labels:
    app: wordpress
spec:
  ports:
    - port: 3306
  selector:
    app: wordpress
    tier: mysql
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: wordpress-mysql
  labels:
    app: wordpress
spec:
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: wordpress
        tier: mysql
    spec:
      containers:
      - image: mysql:8.0.3
        name: mysql
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: password123
        ports:
        - containerPort: 3306
          name: mysql
```

![DNS](https://i.imgflip.com/w2rxh.jpg)