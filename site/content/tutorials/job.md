+++
title = "execute a Job"
weight = 12
+++

A job is a process that will end and signal a healthy end with [exitcode](http://tldp.org/LDP/abs/html/exitcodes.html) 0.

## Docker
To execute date in a container run
```
docker run alpine:3.7 date
```  
In most cases the timezone used in the container differs from your hosts timezone.

## Kubernetes
A job creates a pod to execute the process.
```
apiVersion: batch/v1
kind: Job
metadata:
  name: date
spec:
  template:
    spec:
      containers:
      - name: date
        image: alpine:3.7
        command: ["date"]
      restartPolicy: Never
```
The output is visible via the logs of the pod.
```
kubectl logs -f $(kubectl get pods --show-all --selector=job-name=date --output=jsonpath={.items..metadata.name})
```