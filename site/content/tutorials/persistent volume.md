+++
title = "Persistent Volume"
weight = 9
+++

## Docker

Docker is for history reasons able to use another container as a containers storage. Using this is not recommended.

Docker has the concept of docker managed volumes and of host paths to be mounted into a container.

```
docker run -v $(PWD):/usr/share/nginx/html:ro -p 127.0.0.1:8080:80 nginx
```

`:ro` marks this folder as read only at the target location

## Docker-compose

docker-compose can pass the volume mount information to docker.

```
version: '3.0'

services:
  webserver:
    image: nginx
    ports:
      - '127.0.0.1:8080:80'
    volumes:
      - ".:/usr/share/nginx/html"
```

## Kubernetes

Kubernetes is able to use many [storage providers](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#types-of-persistent-volumes).  
The storage provider and/or the connection to the storage provider is added to the cluster by the administrator.  
The same way a desired deployment gets configured a desire for persistent storage can be expressed.  
This [persistent volume claim](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims) defines the amount and quality class of storage.  

### local volume

Local storage exists in a preconfigured (by administrator changeable) state.  
It persists on a defiend disk, on a defined node up to a defined size.  
When the node with the storage has to few resources to deploy a pod there, the pod will not be started anywhere.  
When a node disapears or a disk breaks the volume is gone. There is no backup.  
On the upside local volumes are able to use the full iops and thoughput of a SSD.

```
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: local-claim-quota
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 5M
  storageClassName: local-storage
---
kind: Pod
apiVersion: v1
metadata:
  name: local-pod-quota
spec:
  containers:
    - name: shell
      image: alpine:3.6
      args: [/bin/sh, -c, 'i=0; while true; do echo "$i: $(date)"; i=$((i+1)); sleep 1; done']
      volumeMounts:
      - mountPath: "/data"
        name: local-data
  volumes:
    - name: local-data
      persistentVolumeClaim:
        claimName: local-claim-quota
```

### rook-block

[Rook](https://rook.io/) is an operator (automated administrator) to setup [ceph](http://ceph.com/).  
A requested volume gets created on the fly with the requested size.  
The data gets replicated on multiple nodes and a pod using the volume is able to run on any node.  
If a node or disk fails ceph will rereplicate to recreate the missing data.  
Requested volumes could be bigger then any single disk of any node.  
The performance is limited by the network in terms of iops and thoughput.  
Even so data is replicated between nodes, this also is not a backup. Deleted data stay deleted.

```
apiVersion: v1
kind: Namespace
metadata:
  name: webdav
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: www
  namespace: webdav
spec:
  storageClassName: rook-block
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: webdav
  namespace: webdav
spec:
  template:
    metadata:
      labels:
        app: webdav
    spec:
      containers:
        - name: webdav
          image: visity/webdav:1.6.2
          livenessProbe:
            httpGet:
              path: /
              port: 80
              scheme: HTTP
          readinessProbe:
            httpGet:
              path: /
              port: 80
              scheme: HTTP
          ports:
            - containerPort: 80
              name: webdav
              protocol: TCP
          volumeMounts:
            - name: www
              mountPath: "/var/www"
          resources:
            limits:
              cpu: 300m
              memory: 128Mi
      volumes:
        - name: www
          persistentVolumeClaim:
            claimName: www
---
apiVersion: v1
kind: Service
metadata:
  name: webdav
  namespace: webdav
spec:
  ports:
    - port: 80
      protocol: TCP
      targetPort: webdav
  selector:
    app: webdav
```

![needs backup](http://s2.quickmeme.com/img/d6/d61b1aa752e5a230b5b91133f9dac762e176c00b1c3f0bc73ad47917f767ba3d.jpg)