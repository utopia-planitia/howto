+++
title = "manage Docker containers"
weight = 2
+++

## start
To execute a interactive shell in a container run
```
docker run --rm -ti alpine:3.7 sh
```
The shell can be exited with `exit`.

To start a container in the background add `-d` 

## status
list currently running containers
```
docker ps
```
list the last 5 started container (including ended)
```
docker ps -n 5
```
list all containers running or stoped
```
docker ps -a
```
show details about a container (entrypoint, command, volumes, ip)
```
docker inspect <id | name>
```

## restart
to restart a container
```
docker restart <id | name>
```

## stop
to stop a running container
```
docker stop <id | name>
```
a stoped container is not listes with the default `docker ps` but still exists and takes up the name

## remove
to remove a stoped container
```
docker rm <id | name>
```

## destroy all containers
```
docker rm -f $(docker ps -a -q)
```

## cleanup
To remove every container, layer, image not used in the last 7 days run  
```
docker system prune -f --filter 'until=168h'
```

![resistance is futile](https://i.imgur.com/miBmupw.png)
