+++
title = "Bare metal vs Virtual mashine vs Container"
linktitle = "Mashine vs Container"
weight = 3
+++

## TL;DR
Containers isolate workloads while maintaining the performance of a physical mashine.

## Comparision

All three compared technics use a operating system to operate a pysical mashine.

![Bare metal servers vs Virtual mashines vs Containers](../metal vs vm vs container.png)


Attribute | Physical Mashine | Virtual Mashine | Containers
--- | --- | --- | ---
Structure | The OS runs Applications and provides the same libraries and resources to all. | The OS runs a Hypervisor. The Hypervisor simulates Hardware per VM. Each VM starts its own guest operating system. The guest OS runs Applications and provides dedicated libraries and resources. | The OS runs a Container Runtime (Docker). The Container Runtime interacts with the Linux Kernel to manage containerize Applications. Each Applications has the option to request dedicated libraries and dedicated resources.
Isolation | None | Each VM runs its own OS and uses its own simulated hardware. | Each Process runs with its own filesystem, process structure and network interface.
Performace | The only layer of abstraction between applications the hardware is the OS. The Performance is good. | Between the application and the hardware are the guest OS, virtual hardware and the main OS. The performance of disks can suffer. CPU and Memory tends to be overprovisioned (underutilized). | The only layer of abstraction between applications the hardware is the OS. The Performance is good.
Startup Time | Application startup time | OS Booting time + Application startup time | Application startup time

![container failure](https://i.pinimg.com/564x/e8/63/2e/e8632eecd1508d39a7eec7c0fdef59b8.jpg)