
include ../kubernetes/etc/help.mk

export SHOW_DRAFTS=false
export SHOW_EXPIRED=false
export SHOW_FUTURE=false

.PHONY: background
background: ##@development start local server in background
	docker-compose up --build -d

.PHONY: start
start: ##@development start local server
	docker-compose up --build

.PHONY: no-hidden
no-hidden: ##@development start local server and include all draft, expired or future pages
	SHOW_DRAFTS=true SHOW_EXPIRED=true SHOW_FUTURE=true docker-compose up --build

.PHONY: stop
stop: ##@development stop local server
	docker-compose stop -t 1
	docker-compose down	--remove-orphans

.PHONY: cli
cli: ##@development open command line interface
	docker-compose exec hugo sh

.PHONY: logs
logs: ##@development show logs
	docker-compose logs -f
